<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pattern_column".
 *
 * @property integer $id
 * @property integer $pattern_id
 * @property string $name
 *
 * @property Pattern $pattern
 * @property RowData[] $rowDatas
 */
class PatternColumn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pattern_column';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pattern_id', 'name'], 'required'],
            [['pattern_id'], 'integer'],
            [['name'], 'string'],
            [['pattern_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::className(), 'targetAttribute' => ['pattern_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pattern_id' => 'Шаблон',
            'name' => 'Имя колонки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPattern()
    {
        return $this->hasOne(Pattern::className(), ['id' => 'pattern_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRowDatas()
    {
        return $this->hasMany(RowData::className(), ['pattern_column_id' => 'id']);
    }
}
