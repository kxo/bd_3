<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "row_data".
 *
 * @property integer $id
 * @property integer $pattern_column_id
 * @property integer $row_id
 * @property string $value
 *
 * @property PatternColumn $patternColumn
 * @property Row $row
 */
class RowData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'row_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pattern_column_id', 'row_id'], 'required'],
            [['pattern_column_id', 'row_id'], 'integer'],
            [['value'], 'string'],
            [['pattern_column_id'], 'exist', 'skipOnError' => true, 'targetClass' => PatternColumn::className(), 'targetAttribute' => ['pattern_column_id' => 'id']],
            [['row_id'], 'exist', 'skipOnError' => true, 'targetClass' => Row::className(), 'targetAttribute' => ['row_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pattern_column_id' => 'Pattern Column ID',
            'row_id' => 'Row ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternColumn()
    {
        return $this->hasOne(PatternColumn::className(), ['id' => 'pattern_column_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRow()
    {
        return $this->hasOne(Row::className(), ['id' => 'row_id']);
    }
}
