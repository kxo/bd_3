<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "row".
 *
 * @property integer $id
 * @property integer $pattern_id
 *
 * @property Pattern $pattern
 * @property RowData[] $rowDatas
 */
class Row extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'row';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pattern_id'], 'required'],
            [['pattern_id'], 'integer'],
            ['primary_data', 'string'],
            [['pattern_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::className(), 'targetAttribute' => ['pattern_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pattern_id' => 'Pattern ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPattern()
    {
        return $this->hasOne(Pattern::className(), ['id' => 'pattern_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRowDatas()
    {
        return $this->hasMany(RowData::className(), ['row_id' => 'id']);
    }
}
