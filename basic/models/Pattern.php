<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pattern".
 *
 * @property string $name
 * @property integer $id
 *
 * @property PatternColumn[] $patternColumns
 * @property Row[] $rows
 */
class Pattern extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pattern';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['start_with'], 'integer'],
            [['extension'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Имя шаблона',
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternColumns()
    {
        return $this->hasMany(PatternColumn::className(), ['pattern_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRows()
    {
        return $this->hasMany(Row::className(), ['pattern_id' => 'id']);
    }
}
