<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $datetime
 */
class Files extends \yii\db\ActiveRecord
{

    /**
     * @var UploadedFile
     */
    public $inputFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url','datetime', 'extension'], 'string'],
            [['inputFile'], 'file', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя файла',
            'url' => 'Путь',
            'datetime'=>'Время загрузки'
        ];
    }
    public function upload()
    {
        if ($this->validate()) {
            $name = 'uploads/'.date('YmdHis'). $this->inputFile->baseName . '.' . $this->inputFile->extension;
            $this->inputFile->saveAs($name);
            $this->name = $this->inputFile->baseName. '.' . $this->inputFile->extension;
            $this->url = $name;
            $this->datetime = date('Y-m-d H:i:s');
            $this->extension = mb_strtolower($this->inputFile->extension);
            $this->inputFile=null;
            if($this->save())
                return true;
            else
                Yii::error(print_r($this->errors,1));
            return false;
        } else {
            Yii::error($this->errors);
            return false;
        }
    }

    /**
     * Должно возвращать сообщение, мол конфликты с тем-то, кем-то. После { confirm(message)?execute():return; }
     * @return string message
     */
    public function pre_execute(){
        return "";
    }

    /**
     * @param $pattern_id
     * @param $sheet
     * @return bool
     * @throws \Exception
     */
    public function execute($pattern_id, $sheet){
        $pattern = Pattern::findOne($pattern_id);
        $patternColumns = PatternColumn::find()->select('id')->where(['pattern_id' => $pattern_id])->orderBy('id')->asArray()->column();
        $validCount = count($patternColumns);
        $parsed = ($pattern->extension == 'csv' ? $this->parseTSV() : $this->parseXLS($sheet));
        if (!$parsed) throw new \Exception('Не удалось распарсить');
        array_splice($parsed,  0,$pattern->start_with - 1);
        Row::deleteAll(['primary_data' => array_map(function($item) {
            return $item[0];
        }, $parsed)]);
//        Row::deleteAll(['pattern_id'=>$pattern_id,'file_id'=>$this->id,'sheet'=>$sheet]);
        foreach ($parsed as $row) {
            Yii::warning($row);
            if (count($row) != $validCount) continue;
            $activeRow = new Row([
                'pattern_id' => $pattern_id,
                'file_id'=>$this->id,
                'sheet'=>$sheet,
                'primary_data' => $row[0]
            ]);
            $activeRow->save();
            foreach ($row as $i => $item) {
                $model = new RowData([
                    'row_id' => $activeRow->id,
                    'pattern_column_id' => $patternColumns[$i],
                    'value' => (string)$item
                ]);
                $model->save();
            }
        }
       return true;
    }


    public function parseTSV() {
//        $row = 1;
        $parsed = [];
        if (($file = fopen(Yii::getAlias('@webroot') . '/' . $this->url, "r")) !== FALSE) {
            while (($data = fgetcsv($file, 0, "\t")) !== FALSE) {
                $parsed[] = $data;
            }
            fclose($file);
            return $parsed;
        }
        return false;
    }
    public function parseXLS($sheet = 0) {
        $objReader = \PHPExcel_IOFactory::createReader('Excel5');
        $objPhpExcel = $objReader->load(Yii::getAlias('@webroot') . '/' . $this->url);
        return $objPhpExcel->getSheet($sheet)->toArray();
    }

}
