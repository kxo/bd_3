<?php

use yii\db\Migration;

class m170612_184434_change_key_row extends Migration
{
    public function up()
    {
        $this->dropForeignKey(
            'row_data_row_FK',
            'row_data'
        );
        $this->addForeignKey(
            'row_data_row_FK',
            'row_data',
            'row_id',
            'row',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170612_184434_change_key_row cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
