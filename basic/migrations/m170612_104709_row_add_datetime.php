<?php

use yii\db\Migration;

class m170612_104709_row_add_datetime extends Migration
{
    public function safeUp()
    {
        $this->addColumn('row','datetime',$this->dateTime()->defaultValue(new \yii\db\Expression('now()')));
    }

    public function safeDown()
    {
        echo "m170612_104709_row_add_datetime cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170612_104709_row_add_datetime cannot be reverted.\n";

        return false;
    }
    */
}
