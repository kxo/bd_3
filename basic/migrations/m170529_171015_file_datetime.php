<?php

use yii\db\Migration;

class m170529_171015_file_datetime extends Migration
{
    public function up()
    {
        $this->addColumn('files','datetime',$this->dateTime());
    }

    public function down()
    {
        echo "m170529_171015_file_datetime cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
