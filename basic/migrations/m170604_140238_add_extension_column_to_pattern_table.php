<?php

use yii\db\Migration;

/**
 * Handles adding extension to table `pattern`.
 */
class m170604_140238_add_extension_column_to_pattern_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('pattern', 'extension', $this->string());
        $this->addColumn('pattern', 'start_with', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('pattern', 'extension');
        $this->dropColumn('pattern', 'start_with');
    }
}
