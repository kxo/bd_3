<?php

use yii\db\Migration;

class m170612_102108_extensionUPDATE extends Migration
{
    public function safeUp()
    {
        $this->update('pattern',['extension'=>new \yii\db\Expression('name')]);
        $this->insert('pattern_column',[
            'pattern_id'=>1,
            'name'=>'ambiguity'
        ]);
    }

    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170612_102108_extensionUPDATE cannot be reverted.\n";

        return false;
    }
    */
}
