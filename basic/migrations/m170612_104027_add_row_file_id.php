<?php

use yii\db\Migration;

class m170612_104027_add_row_file_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('row','file_id',$this->integer());
        $this->addForeignKey('FK_ROW_FILE_ID','row','file_id','files','id');
    }

    public function safeDown()
    {
        echo "m170612_104027_add_row_file_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170612_104027_add_row_file_id cannot be reverted.\n";

        return false;
    }
    */
}
