<?php

use yii\db\Migration;

class m170612_184434_change_key_row2 extends Migration
{
    public function up()
    {
        $this->dropForeignKey(
            'FK_ROW_FILE_ID',
            'row'
        );
        $this->addForeignKey(
            'FK_ROW_FILE_ID',
            'row',
            'file_id',
            'files',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m170612_184434_change_key_row cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
