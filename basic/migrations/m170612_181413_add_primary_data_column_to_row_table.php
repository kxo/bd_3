<?php

use yii\db\Migration;

/**
 * Handles adding primary_data to table `row`.
 */
class m170612_181413_add_primary_data_column_to_row_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('row', 'primary_data', $this->string());
        $this->createIndex(
            'idx-row-primary_data',
            'row',
            'primary_data'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('row', 'primary_data');
    }
}
