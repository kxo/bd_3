<?php

use yii\db\Migration;

/**
 * Handles adding extension to table `files`.
 */
class m170604_193241_add_extension_column_to_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('files', 'extension', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('files', 'extension');
    }
}
