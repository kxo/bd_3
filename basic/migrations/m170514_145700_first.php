<?php

use yii\db\Migration;

class m170514_145700_first extends Migration
{
    public function safeUp()
    {
        $this->createTable('files',[
            'id'=>$this->primaryKey(),
            'name'=>$this->text(),
            'url'=>$this->text(),
        ]);
        $this->createTable('pattern',[
            'name'=>$this->text()->notNull(),
            'id'=>$this->primaryKey()
        ]);
        $this->createTable('pattern_column',[
            'id'=>$this->primaryKey(),
            'pattern_id'=>$this->integer()->notNull(),
            'name'=>$this->text()->notNull()
        ]);
        $this->addForeignKey('pattern_column_pattern_FK','pattern_column','pattern_id','pattern','id');
        $this->createTable('row',[
            'id'=>$this->primaryKey(),
            'pattern_id'=>$this->integer()->notNull()
        ]);
        $this->addForeignKey('row_pattern_FK','row','pattern_id','pattern','id');

        $this->createTable('row_data',[
            'id'=>$this->primaryKey(),
            'pattern_column_id' => $this->integer()->notNull(),
            'row_id'=>$this->integer()->notNull(),
            'value'=>$this->text()
        ]);
        $this->addForeignKey('row_data_pattern_column_FK','row_data','pattern_column_id','pattern_column','id');
        $this->addForeignKey('row_data_row_FK','row_data','row_id','row','id');

        $this->insert('pattern',[
            'name'=>'csv'
        ]);
        $this->batchInsert('pattern_column',['pattern_id','name'],[
            [1,'Filename'],
            [1,'Lexical Units'],
            [1,'Wordforms'],
            [1,'ambiguity'],
            [1,'NR']
        ]);

    }

    public function down()
    {
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
