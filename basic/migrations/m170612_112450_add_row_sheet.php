<?php

use yii\db\Migration;

class m170612_112450_add_row_sheet extends Migration
{
    public function safeUp()
    {
        $this->addColumn('row','sheet',$this->integer());
    }

    public function safeDown()
    {
        echo "m170612_112450_add_row_sheet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170612_112450_add_row_sheet cannot be reverted.\n";

        return false;
    }
    */
}
