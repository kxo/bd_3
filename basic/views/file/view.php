<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Files */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Файлы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$pattern = Yii::$app->request->get('pattern_id', null);
$sheet = Yii::$app->request->get('sheet', 0);
?>
<div class="files-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <div class="col-sm-3"><?= \yii\bootstrap\Html::dropDownList('pattern', $pattern, \app\models\Pattern::find()->where(['extension' => $model->extension])->indexBy('id')->column(), ['class' => 'form-control', 'id' => 'pattern']) ?></div>
        <? echo '<div class="col-sm-2">' . Html::input('number', null, $sheet, [
                'class' => 'form-control', 'id' => 'sheet',
                'disabled'=>($model->extension != 'xls')
            ]) . "</div>"?>
        <?= Html::a('Превью', ['#'], ['class' => 'btn btn-primary', 'id' => 'preview']) ?>
        <?= Html::a('Обработать', ['#'], ['class' => 'btn btn-primary', 'id' => 'execute', 'data-id' => $model->id]) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name:ntext',
            'url:ntext',
            'datetime:ntext'
        ],
    ]) ?>
    <?php

        if ($pattern) {
    ?>
            <table class="table table-bordered table-striped">
                <?php
                $pattern = \app\models\Pattern::find()->where(['id' => $pattern])->with('patternColumns')->asArray()->one();
                echo '<thead><tr>';
                foreach ($pattern['patternColumns'] as $item) {
                    echo '<th>' . $item['name'] . '</th>';
                }
                echo '</tr></thead>';
                $validCount = count($pattern['patternColumns']);
                $parsed = ($pattern['extension'] == 'csv' ? $model->parseTSV() : $model->parseXLS($sheet));
                if ($parsed) {
                    array_splice($parsed, 0, $pattern['start_with'] - 1);
                    foreach ($parsed as $row) {
                        $class = \app\models\Row::find()->where(['primary_data' => $row[0]])->exists() ?
                            'warning' : '';
                        $class = count($row) != $validCount ? 'danger' : $class;
                        echo "<tr class=\"$class\">";
                        foreach ($row as $item) {
                            echo '<td>' . $item . '</td>';
                        }
                        echo '</tr>';
                    }
                }
                ?>
            </table>
    <?php
        }
    ?>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var $execute = $('#execute');
            $('#execute').click(function (e) {
                e.preventDefault();
                $.get('/file/execute?id=' + $execute.attr('data-id') + '&pattern_id=' + pattern.value + '&sheet=' + sheet.value || '', function (data) {
                    alert('Успех');
                })
            }).error(function () {
                alert('Ошибка');
            });
            $('#preview').click(function (e) {
                e.preventDefault();
                var url = new URL(location.href);
                url.searchParams.set('pattern_id', pattern.value);
                if (typeof sheet != "undefined" && sheet.value) url.searchParams.set('sheet', sheet.value);
                location.href = url.href;
            })
        })
    </script>

</div>
