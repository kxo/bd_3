<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pattern */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pattern-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить колонку', ['create-column','pattern_id'=>$model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Просмотр данных', ['row/index','pattern_id'=>$model->id], ['class' => 'btn btn-primary']) ?>

    </p>
    <?php
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => \app\models\PatternColumn::find()->where(['pattern_id'=>$model->id])->orderBy('id DESC'),
        'pagination' => false,
    ]);
?>
    <div class="list-group">
    <?php
        echo \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list',
        ]);
    ?>
    </div>



</div>
