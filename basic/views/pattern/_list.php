<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;


?>
<button  class="list-group-item">
    <?= Html::encode($model->name) ?>
    <a  href="<?= \yii\helpers\Url::to(['delete-column','column_id'=>$model->id]) ?>" class="pull-right">x</a>
</button>
