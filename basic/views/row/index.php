<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $file \app\models\Files */
/* @var $columns array */

$this->title = 'Записи файла '.$file->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => array_merge([
            ['class' => 'yii\grid\SerialColumn'],
        ],$columns,[])
    ]); ?>
</div>
