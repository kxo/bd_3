<?php

namespace app\controllers;

use app\models\Files;
use app\models\Pattern;
use Yii;
use app\models\Row;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RowController implements the CRUD actions for Row model.
 */
class RowController extends Controller
{
    public function actionIndex($pattern_id)
    {
        $columns = [];
        $pattern = Pattern::findOne($pattern_id);
        $query = Row::find()
            //->orderBy(['row.datetime'=>SORT_DESC])
            ->asArray();
        $query->andWhere(['pattern_id'=>$pattern_id]);
        $query->leftJoin('files','files.id = row.file_id');
        foreach ($pattern->patternColumns as $column){
            $query->addSelect([
                $column->name=>new Expression("( SELECT VALUE from row_data where row_data.pattern_column_id = $column->id and row_id = row.id)")
            ]);
            $columns[]=$column->name;
        }
        $query->addSelect(['row.datetime']);
        $query->addSelect(['file_id'=>'files.id']);
        $query->addSelect(['id'=>'row.id']);

        $query->addSelect(['file_name'=>'files.name']);
        $columns[] = 'datetime';
        $columns[] = [
            'attribute'=>'file_name',
            'format'=>'raw',
            'value'=>function($data){
                return Html::a($data['file_name'],['file/view','id'=>$data['file_id']]);
            }
        ];


        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'pattern'=>$pattern,
            'columns'=>$columns
        ]);
    }
}
